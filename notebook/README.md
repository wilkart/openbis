---
title: openBIS Cloud
---

# openBIS

**[openBIS](https://openbis.ch/)** is a framework designed for managing and analyzing research data. It is an open-source platform developed primarily for the life sciences community to handle large volumes of experimental data generated in fields such as genomics, proteomics, and imaging. Overall, **openBIS** (**open Biology Information System**) aims to streamline the management and analysis of research data, facilitating collaboration and accelerating scientific discovery.


# Cloud

**[Cloud](https://en.wikipedia.org/wiki/Cloud_computing)** computing is the on-demand availability of computer system resources, especially data storage (cloud storage) and computing power. Large clouds often have functions distributed over multiple locations, each of which is a data center. **Cloud** computing relies on sharing of resources and storage to achieve coherence with improved manageability and less maintenance.


# Repository

Source code and open issues are stored on [GitLab](https://gitlab.com/wilkart/openbis).


# Credits

**Artur Pędziwilk** - Systems Specialist, Scientific IT Services of ETH Zürich for providing Oracle Cloud Infrastructure for development and testing.

**Beat Christen** - Professor, Head Institute of Microbiology at University of Stuttgart for providing Amazon Web Services cloud computing infrastructure for development and testing.
