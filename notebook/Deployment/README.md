---
title: Deployment
---

**Docker** is a platform to automate deployments of applications inside lightweight, portable containers. 
Containers are a form of lightweight virtualization that packages an application and its dependencies together. 

**Podman** is a daemonless, open source tool designed to make it easy to find, run, build, 
share and deploy applications using containers and container images.

**Kubernetes** is an open-source container orchestration platform designed to automate the deployment, scaling, and management of containerized applications. Kubernetes is a de facto standard for container orchestration in the world of containerized applications.

