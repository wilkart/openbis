---
title: Docker
---

# Provisioning

## Docker Network

```bash
docker network create openbis-tier --driver bridge;
```

## Docker Containers

### openbis-db

```bash
docker run --detach --replace \
  --name openbis-db \
  --hostname openbis-db \
  --network openbis-tier \
  -v openbis-db-data:/var/lib/postgresql/data \
  -e POSTGRES_PASSWORD="eiBoh0Ae" \
  -e PGDATA=/var/lib/postgresql/data/pgdata \
  --restart unless-stopped \
  postgres:15-alpine;
```

### openbis-app

```bash
docker run --detach --replace \
  --name openbis-app \
  --hostname openbis-app \
  --network openbis-tier \
  --pid host \
  -p 127.0.0.1:8080:8080 \
  -p 127.0.0.1:8081:8081 \
  -v openbis-app-data:/data \
  -v openbis-app-etc:/etc/openbis \
  -v openbis-app-logs:/var/log/openbis \
  -e OPENBIS_ADMIN_PASS="ohloo0Be" \
  -e OPENBIS_DATA="/data/openbis" \
  -e OPENBIS_DB_ADMIN_PASS="eiBoh0Ae" \
  -e OPENBIS_DB_ADMIN_USER="postgres" \
  -e OPENBIS_DB_APP_PASS="eiL8aigh" \
  -e OPENBIS_DB_APP_USER="openbis" \
  -e OPENBIS_DB_HOST="openbis-db" \
  -e OPENBIS_ETC="/etc/openbis" \
  -e OPENBIS_HOME="/home/openbis" \
  -e OPENBIS_LOG="/var/log/openbis" \
  -e OPENBIS_FQDN="openbis.cloud" \
  -e enabled-modules="monitoring-support, dropbox-monitor, dataset-uploader, dataset-file-search, xls-import, search-store, openbis-sync, admin, eln-lims, microscopy" \
  osgiliath/openbis:6.6;
```

### openbis-ingress

```bash
true;
```

# Verification

## Docker

```bash
docker inspect openbis-tier;
docker inspect openbis-db;
docker inspect openbis-app;
docker inspect openbis-ingress;
```

## openBIS database

```bash
true;
```

## openBIS application servers

```bash
true;
```

## openBIS ingress

```bash
true;
```
