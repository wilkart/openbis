---
title: Introduction
---

# Introduction

The open source platform **openBIS - Open Biology Information System** - offers an *Electronic Laboratory Notebook* and a *Laboratory Information Management System* **(ELN-LIMS)** solution suitable for the academic life science laboratories. **openBIS ELN-LIMS** allows researchers to efficiently document their work, to describe materials and methods, and to collect raw and analysed data and analysis scripts.

The openBIS ELN-LIMS platform is developed by [Scientific IT Services in ETH Zürich](https://openbis.ch/index.php/our-team/).


# openBIS Cloud

**openBIS Cloud** is a self-contained project aimed at facilitating learning and experimentation with cloud infrastructure specifically chosen for openBIS.
The project's objective is to offer concise quickstart guides and functional solutions utilizing the most aduequate technologies.
No warranty is provided, and all materials within the project are intended solely for demonstration and reference purposes.
