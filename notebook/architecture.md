---
title: Architecture
---

# Architecture

**openBIS** architecture is highly scalable and capable of storing and providing access to large volumes of data as a distributed storage. 
All raw data and result data are stored in the **[datastore](/architecture#datastore)**, which is a managed flat-file system. 
openBIS utilizes a hybrid data repository, where the filesystem data store is augmented with a relational **[database](/architecture#database)** management system for indexing information, metadata, and selected results. 
Access - **[ingress](/architecture#ingress)** - to the service is provided through web browser as a reverse proxy. 
Basic openBIS **[deployment](/architecture#deployment)** consists of two Java **[application servers](/architecture#servers)** - the **Application Server** (AS) and the **Data Store Server** (DSS). 
The **AS** manages the metadata and links to the data while the **DSS** manages the data itself.

## Deployment

Schema A1 - openBIS deployment:

```mermaid
stateDiagram-v2
    direction LR
    [*] --> INGRESS
    INGRESS --> SERVERS
    SERVERS --> DATABASE
    state SERVERS {
      direction LR
      DSS --> AS
      AS --> DSS
    }
    SERVERS --> DATASTORE
```

Schema A2 - openBIS connectivity:

```mermaid
stateDiagram-v2
    direction LR
    [*] --> 443/tcp: https
    443/tcp --> SERVERS: https
    SERVERS --> 5432/tcp: sql
    state SERVERS {
      direction LR
      8081/tcp --> 8080/tcp: api
      8080/tcp --> 8081/tcp: api
    }
    SERVERS --> filesystem: io
```

Schema A3 - openBIS containers:

```mermaid
stateDiagram-v2
    direction LR
    [*] --> ingress
    ingress --> app
    app --> db
```

### Database

RDMS (postgres - 5432/tcp)


### Application Servers

AS (java - 8080/tcp)

DSS (java - 8081/tcp)


### Ingress

INGRESS (haproxy - 443/tcp)


## Datastore

Datastore
