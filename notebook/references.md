---
title: References
---

# References

## openBIS

* [openBIS ELN-LIMS: an open-source database for academic laboratories.](https://doi.org/10.1093/bioinformatics/btv606)
* [openBIS: a flexible framework for managing and analyzing complex data in biology research.](https://doi.org/10.1186/1471-2105-12-468)

## Containerization

* [Docker (software)](https://en.wikipedia.org/wiki/Docker_(software))

## Databases

* [PostgreSQL](https://en.wikipedia.org/wiki/PostgreSQL)

## Providers

* [Oracle Cloud Infrastructure Container Instances](https://www.oracle.com/cloud/cloud-native/container-instances/)

* [Amazon Elastic Container Service](https://aws.amazon.com/ecs/)
