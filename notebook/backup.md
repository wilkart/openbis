---
title: Backup
---

# Database

```bash
docker exec openbis-db pg_dumpall -U postgres -c --if-exists --exclude-database=postgres --exclude-database=template0 --exclude-database=template1 | gzip -c > openbis-db.sql.gz;
```

# Configuration

```bash
docker exec openbis-app tar --one-file-system -C /etc -cf - openbis | gzip -c > openbis-app-etc.tar.gz;
docker exec openbis-app tar --one-file-system -C /etc/openbis/as -cf - passwd | gzip -c > openbis-app-passwd.tar.gz;
```

# Datastore

```bash
docker exec openbis-app tar --one-file-system -C /data -cf - openbis | gzip -c > openbis-app-data.tar.gz;
```
